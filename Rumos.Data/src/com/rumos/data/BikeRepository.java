package com.rumos.data;

import java.util.List;

import com.rumos.model.Bike;

public interface BikeRepository {
	List<Bike> findAll();

	Bike findById(Integer id);

	Bike add(Bike bike);

	Bike update(Bike bike);

	void remove(Integer id);

}
