package com.rumos.data;

import java.util.ArrayList;
import java.util.List;

import com.rumos.model.Bike;

public class BikeRepositoryImpl implements BikeRepository {
	private Bike[] bikes = new Bike[3];
	private static int id;
	
	@Override
	public List<Bike> findAll() {
		List<Bike> bikes = new ArrayList<Bike>();
		for (int i = 0; i < this.bikes.length; i++) {
			if(this.bikes[i] != null) {
				bikes.add(this.bikes[i]);
			}
		}
		return bikes;
	}

	@Override
	public Bike findById(Integer id) {
		for (int i = 0; i < bikes.length; i++) {
			if(bikes[i] == null) {
				continue;
			}
			if(bikes[i].getId().equals(id)) {
				return bikes[i];
			}
		}
		throw new RuntimeException("BIKE NOT FOUND");
	}

	@Override
	public Bike add(Bike bike) {
		for (int i = 0; i < bikes.length; i++) {
			if(bikes[i] == null) {
				id++;
				bike.setId(id);
				bikes[i] = bike;
				return bikes[i];
			}
		}
		throw new RuntimeException("DATABASE FULL");
	}

	@Override
	public Bike update(Bike bike) {
		for (int i = 0; i < bikes.length; i++) {
			if(bikes[i] == null) continue;
			if(bikes[i].getId().equals(bike.getId())) {
				bikes[i] = bike;
				return bikes[i];
			}
			
		}
		throw new RuntimeException("BIKE NOT FOUND");
	}

	@Override
	public void remove(Integer id) {
		for (int i = 0; i < bikes.length; i++) {
			if (bikes[i] == null) continue;
			if (bikes[i].getId().equals(id)) {
				bikes[i] = null;
				return;
			}
		}
		throw new RuntimeException("BIKE NOT FOUND");

	}

}
