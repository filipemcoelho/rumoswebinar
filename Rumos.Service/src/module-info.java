module com.rumos.service {
	
	requires transitive com.rumos.model;
	requires com.rumos.data;
	
	exports com.rumos.service;
}