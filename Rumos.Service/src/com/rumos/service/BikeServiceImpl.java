package com.rumos.service;

import java.time.LocalDate;
import java.util.List;

import com.rumos.data.BikeRepository;
import com.rumos.data.BikeRepositoryImpl;
import com.rumos.model.Bike;

public class BikeServiceImpl implements BikeService {
	private BikeRepository bikeRepo = new BikeRepositoryImpl();

	@Override
	public List<Bike> findAll() {
		return bikeRepo.findAll();
	}

	@Override
	public Bike findByid(Integer id) {
		return bikeRepo.findById(id);
	}

	@Override
	public Bike add(Bike bike) {
		checkFutureManufacture(bike);
		return bikeRepo.add(bike);
	}

	@Override
	public Bike update(Bike bike) {
		checkFutureManufacture(bike);
		return bikeRepo.update(bike);
	}

	@Override
	public void remove(Integer id) {
		bikeRepo.remove(id);
	}
	
	private void checkFutureManufacture(Bike bike) {
		if(bike.getManufacture().isAfter(LocalDate.now())) {
			throw new RuntimeException("Cannot be built in the future");
		}
	}

}
