package com.rumos.service;

import java.util.List;

import com.rumos.model.Bike;

public interface BikeService {

	List<Bike> findAll();

	Bike findByid(Integer id);

	Bike add(Bike bike);

	Bike update(Bike bike);

	void remove(Integer id);

}
