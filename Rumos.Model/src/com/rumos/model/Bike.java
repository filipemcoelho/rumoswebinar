package com.rumos.model;

import java.time.LocalDate;

public class Bike {

	private Integer id;
	private String brand;
	private Boolean gears;
	private Double price;
	private Type type;
	private LocalDate manufacture;

	public Bike() {
	}

	public Bike(String brand, Boolean gears, Double price, Type type, LocalDate manufacture) {
		super();
		this.brand = brand;
		this.gears = gears;
		this.price = price;
		this.type = type;
		this.manufacture = manufacture;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public Boolean isGears() {
		return gears;
	}

	public void setGears(Boolean gears) {
		this.gears = gears;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public LocalDate getManufacture() {
		return manufacture;
	}

	public void setManufacture(LocalDate manufacture) {
		this.manufacture = manufacture;
	}

	@Override
	public String toString() {
		return "Bike [id=" + id + ", brand=" + brand + ", gears=" + gears + ", price=" + price + ", type=" + type
				+ ", manufacture=" + manufacture + "]";
	}

}
