package com.rumos.console;

import java.time.LocalDate;
import java.util.List;
import java.util.Scanner;

import com.rumos.model.Type;
import com.rumos.model.Bike;
import com.rumos.service.BikeService;
import com.rumos.service.BikeServiceImpl;

public class BikeStore {

	private BikeService service;
	private Scanner scanner;

	public BikeStore() {
		service = new BikeServiceImpl();
		scanner = new Scanner(System.in);
	}

	public void startApp() {

		boolean exit = false;

		welcome();

		do {
			menu();
			System.out.print("Enter option: ");
			int option = scanner.nextInt();
			scanner.nextLine();

			switch (option) {
			case 1:
				listAllBikes();
				break;
			case 2:
				findBike();
				break;
			case 3:
				addNewBike();
				break;
			case 4:
				updateBike();
				break;
			case 5:
				removeBike();
				break;
			case 0:
				System.out.println("Thank you.");
				exit = true;
				break;
			}
		} while (!exit);
	}

	private void welcome() {
		System.out.println("**********************************");
		System.out.println("*** Welcome to your bike store ***");
		System.out.println("**********************************");
	}

	private void menu() {
		System.out.println("1. List all bikes");
		System.out.println("2. Find bike");
		System.out.println("3. Add a new bike");
		System.out.println("4. Update a bike");
		System.out.println("5. Remove a bike");
		System.out.println("0. Quit application");
	}

	private void listAllBikes() {
		List<Bike> list = service.findAll();

		list.stream().forEach(System.out::println);
	}

	private void findBike() {
		listAllBikes();

		System.out.print("Enter bike id: ");
		int id = scanner.nextInt();
		scanner.nextLine();

		Bike bike = service.findByid(id);
		System.out.println("Here's selected bike details");
		System.out.println(bike + "\n");
	}

	private void addNewBike() {
		System.out.print("Enter bike brand: ");
		String brand = scanner.nextLine();

		System.out.print("Bike has gears (y/n): ");
		String gears = scanner.nextLine();
		boolean hasGears = gears.equalsIgnoreCase("y") ? true : false;

		System.out.print("Enter bike price: ");
		double price = scanner.nextDouble();

		for (Type type : Type.values()) {
			System.out.println(type.ordinal() + " " + type);
		}
		System.out.print("Enter option: ");
		int typeOption = scanner.nextInt();
		scanner.nextLine();

		Type type = Type.values()[typeOption];

		System.out.print("Enter manufacture date (yyyy-MM-dd): ");
		LocalDate date = LocalDate.parse(scanner.nextLine());

		Bike bike = new Bike(brand, hasGears, price, type, date);

		service.add(bike);
//		System.out.println(bike);
	}

	private void updateBike() {
		listAllBikes();

		System.out.print("Enter bike id to update: ");
		int id = scanner.nextInt();
		scanner.nextLine();

		Bike bike = service.findByid(id);

		System.out.println("1. Update brand ");
		System.out.println("2. Update gears");
		System.out.println("3. Update price");
		System.out.println("4. Update type");
		System.out.println("5. Update manufacture date");
		System.out.print("Enter option: ");
		int option = scanner.nextInt();
		scanner.nextLine();

		switch (option) {
		case 1:
			System.out.print("Enter new brand: ");
			String brand = scanner.nextLine();
			bike.setBrand(brand);
			break;
		case 2:
			System.out.print("Bike has gears (y/n): ");
			String gears = scanner.nextLine();
			boolean hasGears = gears.equalsIgnoreCase("y") ? true : false;
			bike.setGears(hasGears);
			break;
		case 3:
			System.out.print("Enter bike price: ");
			double price = scanner.nextDouble();
			bike.setPrice(price);
			break;
		case 4:
			for (Type type : Type.values()) {
				System.out.println(type.ordinal() + " " + type);
			}
			System.out.print("Enter option: ");
			int typeOption = scanner.nextInt();
			scanner.nextLine();
			bike.setType(Type.values()[typeOption]);
			break;
		case 5:
			System.out.print("Enter new date (yyyy/MM/dd): ");
			LocalDate date = LocalDate.parse(scanner.nextLine());
			bike.setManufacture(date);
			break;
		}

		service.update(bike);
	}

	private void removeBike() {
		listAllBikes();

		System.out.print("Enter bike id to remove: ");
		int id = scanner.nextInt();
		scanner.nextLine();

		System.out.print("Are you sure you want to remove the selected bike (y/n)?");
		String choice = scanner.nextLine();

		if (choice.equalsIgnoreCase("y")) {
			service.remove(id);
			System.out.println("Bike removed with sucess");
		} else {
			System.out.println("Bike wasn't removed");
		}
	}
}
